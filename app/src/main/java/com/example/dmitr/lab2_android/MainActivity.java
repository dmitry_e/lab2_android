package com.example.dmitr.lab2_android;

import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView tvInfo;
    EditText etInput;
    Button bControl;
    ConstraintLayout view;
    Spinner complexity;
    TextView tvCounter;
    int number, intComplexity, counter = 0;
    List<Integer> steps = new ArrayList<>();
    Random rand = new Random(System.currentTimeMillis());
    List<String> complexities = new ArrayList<>();
    String currentComplexity;


    private void initializeById(){
        tvCounter = (TextView) findViewById(R.id.counter);
        tvInfo = (TextView) findViewById(R.id.textView);
        etInput = (EditText) findViewById(R.id.editText);
        bControl = (Button) findViewById(R.id.button);
        complexity = (Spinner) findViewById(R.id.complexity);
        view = (ConstraintLayout) findViewById(R.id.view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeById();

        complexities.add(getResources().getString(R.string.complexity1));
        complexities.add(getResources().getString(R.string.complexity2));
        complexities.add(getResources().getString(R.string.complexity3));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, complexities);
        complexity.setAdapter(adapter);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentComplexity = (String) parent.getItemAtPosition(position);
                replay();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currentComplexity = complexities.get(0);
                replay();
            }
        };
        complexity.setOnItemSelectedListener(itemSelectedListener);

        currentComplexity = complexities.get(0);
        replay();
    }

    private void checkIfGues(){
        Resources resources = getResources();
        int try_number = Integer.valueOf(etInput.getText().toString());
        if (!steps.contains(try_number)) {
            steps.add(try_number);
            counter++;
            tvCounter.setText(String.valueOf(counter));
        }
        if (try_number > intComplexity || try_number < 1) {
            tvInfo.setText(resources.getString(R.string.error));
            view.setBackgroundColor(getResources().getColor(R.color.colorPressedHighlight));
        } else {
            if (try_number == number) {
                tvInfo.setText(resources.getString(R.string.hit));
                bControl.setText(resources.getString(R.string.play_more));
                view.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
            } else if (try_number < number) {
                tvInfo.setText(resources.getString(R.string.behind));
                view.setBackgroundColor(getResources().getColor(R.color.colorDarkRed));
            } else if (try_number > number) {
                tvInfo.setText(resources.getString(R.string.ahead));
                view.setBackgroundColor(getResources().getColor(R.color.colorDarkRed));
            }
        }
    }

    public void onClick(View v) {
        Resources resources = getResources();
        if (bControl.getText() == resources.getString(R.string.play_more)) {
            replay();
        } else {
            try {
                checkIfGues();
            } catch (NumberFormatException e) {
                tvInfo.setText(resources.getString(R.string.error));
                view.setBackgroundColor(getResources().getColor(R.color.colorPressedHighlight));
            }
        }
    }

    private void generate() {
        do {
            number = Math.abs(rand.nextInt() % intComplexity);
        }
        while (number == 0);
        tvInfo.setText(getResources().getString(R.string.try_to_guess));
    }

    private void setCurrentComplexity() {
        intComplexity = Integer.parseInt(currentComplexity.substring(4));
    }

    private void replay(){
        Resources resources = getResources();
        setCurrentComplexity();
        generate();
        steps.removeAll(steps);
        counter = 0;
        tvCounter.setText(String.valueOf(counter));
        bControl.setText(resources.getString(R.string.input_value));
        tvInfo.setText(resources.getString(R.string.try_to_guess));
        view.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
    }
}
